#pragma once

class Stack {

	void* position;
	void* data;

public:

	Stack() {
		data = new char[1024];
		position = data;
	}

	~Stack() {
		delete[] data;
	}

	void* getData() {
		return data;
	}

	void* peek() {
		return position;
	}

	long pop_long() {
		position = (long*)position - 1;
		return *(long*)position;
	}

	void push_long(long val) {
		*(long*)position = val;
		position = (long*)position + 1;
	}

	long peek_long(unsigned long offset) {
		return *((long*)position - offset);
	}
};