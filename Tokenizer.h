#pragma once

#include <cstring>

class String {
	char* string;

public:
	String(char* string, unsigned int length) {

		this->string = new char[length + 1];

		memcpy(this->string, string, length);

		this->string[length] = 0;
	}

	~String() {
		delete[] string;
	}

	operator char* () {
		return this->string;
	}
};

struct Tokenizer {
	char* string;
	unsigned int stringLength;

	char* delimeters;
	unsigned int numDelimeters;

	unsigned int offset;
	unsigned int tokenLength;

	Tokenizer(char* string, unsigned int stringLength, char* delimeters, unsigned int numDelimeters) {
		setString(string, stringLength);
		setDelimeters(delimeters, numDelimeters);

		setOffset(0);
		tokenLength = 0;
	}

	void setDelimeters(char* delimeters, unsigned int numDelimeters) {
		this->delimeters = delimeters;
		this->numDelimeters = numDelimeters;
	}

	void setString(char* string, unsigned int stringLength) {
		this->string = string;
		this->stringLength = stringLength;
	}

	void setOffset(unsigned int offset) {
		this->offset = offset;
	}

	int getTokenLength() {
		return tokenLength;
	}

	int compareToken(const char* string) {
		//if (strlen(string) != tokenLength)
		//	return false;

		for (int i = 0; i < tokenLength; i++)
			if (string[i] != this->string[offset - tokenLength + i])
				return false;

		return true;
	}

	int compareDelimeters(const char c) {
		for (int i = 0; i < numDelimeters; i++)
			if (c == delimeters[i])
				return true;

		return false;
	}

	int next() {

		while (compareDelimeters(string[offset]) && offset < stringLength) ++offset;

		tokenLength = offset;

		while (!compareDelimeters(string[offset]) && offset < stringLength) ++offset;

		tokenLength = offset - tokenLength;

		return tokenLength;
	}

	int nextEmpty() {

		if (compareDelimeters(string[offset])) {
			++offset;

			tokenLength = 1;

			return tokenLength;
		}

		tokenLength = offset;

		while (!compareDelimeters(string[offset]) && offset < stringLength) ++offset;

		tokenLength = offset - tokenLength;

		return tokenLength;
	}

	int next(const char delimeter) {
		tokenLength = offset;

		while (string[offset] != delimeter && offset < stringLength)
			++offset;

		tokenLength = offset - tokenLength;

		return tokenLength;
	}

	String tokenToString() {
		return String(string + offset - tokenLength, tokenLength);
	}
};