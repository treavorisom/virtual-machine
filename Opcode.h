#pragma once

enum Opcode {
	end = 0,

	ipop,
	ipush,
	iconst,
	ival,
	imov,

	iadd, isub,
	imul, idiv, imod,
	iinc, idec,
	iand, ior, inot,
	ineg,

	iles, igre, iequ, ieles, iegre, ineq,

	jmp, con,
};