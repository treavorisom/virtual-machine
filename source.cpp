#include <fstream>
#include <iostream>
#include <iomanip>
#include <chrono>

#include "Opcode.h"
#include "Stack.h"
#include "Tokenizer.h"

int compile();

void main() {

	compile();

	unsigned long filesize;
	long* program;

	// Load the bytecode
	std::ifstream file("bytecode.bin", std::ios::in | std::ios::binary | std::ios::ate);
	
	if (file.is_open()) {
	
		filesize = file.tellg();
		program = (long*)(new unsigned char[filesize]);
	
		file.seekg(0, std::ios::beg);
	
		file.read((char*)program, filesize);
	
		file.close();

	} else {

		std::cout << "Error reading file. ";
		system("pause");
		return;
	}

	// Initialize the virtual machine

	unsigned int pc = 0;        // program counter
	char* mem = new char[1024]; // memory
	Stack stack;                // stack

	auto start = std::chrono::high_resolution_clock::now(); // Keep track of execution time

	// main loop
	while (program[pc]) {

		switch (program[pc]) {
			case Opcode::ipop:
			{
				long address = stack.pop_long();
				long val = stack.pop_long();
				*(long*)(mem + address) = val;
				break;
			}

			case Opcode::ipush:
				stack.push_long(*(long*)(mem + stack.pop_long()));
				break;

			case Opcode::iconst:
			{
				long iconst = program[++pc];
				stack.push_long(iconst);
				break;
			}

			case Opcode::imov:
			{
				long* a = (long*)((char*)mem + stack.pop_long());
				long* b = (long*)((char*)mem + stack.pop_long());
				*b = *a;
				break;
			}

			case Opcode::ival:
			{
				long address = stack.pop_long();
				long memvalue = *(long*)((char*)mem + address);
				long value = *(long*)((char*)mem + memvalue);
				stack.push_long(value);
				break;
			}

			case Opcode::iadd:
			{
				long b = stack.pop_long();
				long a = stack.pop_long();
				stack.push_long(a + b);
				break;
			}

			case Opcode::isub:
			{
				long b = stack.pop_long();
				long a = stack.pop_long();
				stack.push_long(a - b);
				break;
			}

			case Opcode::imul:
			{
				long b = stack.pop_long();
				long a = stack.pop_long();
				stack.push_long(a * b);
				break;
			}

			case Opcode::idiv:
			{
				long b = stack.pop_long();
				long a = stack.pop_long();
				stack.push_long(a / b);
				break;
			}

			case Opcode::imod:
			{
				long b = stack.pop_long();
				long a = stack.pop_long();
				stack.push_long(a % b);
				break;
			}

			case Opcode::iinc:
			{
				long a = stack.pop_long();
				stack.push_long(a++);
				break;
			}

			case Opcode::idec:
			{
				long a = stack.pop_long();
				stack.push_long(a++);
				break;
			}

			case Opcode::ineg:
			{
				long a = stack.pop_long();
				stack.push_long(-a);
				break;
			}

			case Opcode::inot:
			{
				long a = stack.pop_long();
				stack.push_long(!a);
				break;
			}

			case Opcode::iand:
			{
				long b = stack.pop_long();
				long a = stack.pop_long();
				stack.push_long(a & b);
				break;
			}

			case Opcode::ior:
			{
				long b = stack.pop_long();
				long a = stack.pop_long();
				stack.push_long(a | b);
				break;
			}

			case Opcode::igre:
			{
				long b = stack.pop_long();
				long a = stack.pop_long();
				stack.push_long(a > b);
				break;
			}

			case Opcode::iles:
			{
				long b = stack.pop_long();
				long a = stack.pop_long();
				stack.push_long(a < b);
				break;
			}

			case Opcode::iequ:
			{
				long b = stack.pop_long();
				long a = stack.pop_long();
				stack.push_long(a == b);
				break;
			}

			case Opcode::ieles:
			{
				long b = stack.pop_long();
				long a = stack.pop_long();
				stack.push_long(a <= b);
				break;
			}

			case Opcode::iegre:
			{
				long b = stack.pop_long();
				long a = stack.pop_long();
				stack.push_long(a >= b);
				break;
			}

			case Opcode::ineq:
			{
				long b = stack.pop_long();
				long a = stack.pop_long();
				stack.push_long(a != b);
				break;
			}

			case Opcode::jmp:
			{
				pc = stack.pop_long();
				continue;
			}

			case Opcode::con:
			{
				long address = stack.pop_long();
				long cond = stack.pop_long();
				if (cond) {
					pc = address;
					continue;
				}
				break;
			}
			default:
				break;
		}

		++pc;
	}

	// Calculate execution time
	auto elapsed = std::chrono::high_resolution_clock::now() - start;
	long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();

	std::cout << "\nProgram Terminated         ";
	std::cout << "Execution Time: " << microseconds << '\n';

	// Print stack memory
	std::cout << "\n-------------------------- Stack Values -------------------------\n";

	for (int i = 0; i < 36; i++)
		std::cout << (i % 6 == 0 ? '\n' : ' ') << std::setw(10) << *(((long*)stack.getData()) + i);

	std::cout << "\n\n";

	// Print program memory
	std::cout << "\n-------------------------- Memory Values ------------------------\n";

	for (int i = 0; i < 36; i++)
		std::cout << (i % 6 == 0 ? '\n' : ' ') << std::setw(10) << *((int*)mem + i);

	std::cout << "\n\n";

	// Cleanup
	delete[] program;
	delete[] mem;

	// End
	std::cout << '\n'; system("pause");
}

int compile() {
	// Get the bytecode filename
	char* filename = new char[128];
	std::cout << "File to compile: "; std::cin >> filename;

	unsigned long filesize;
	char* source;

	// Load the bytecode
	std::ifstream file(filename, std::ios::in | std::ios::binary | std::ios::ate);

	if (file.is_open()) {

		filesize = file.tellg();
		source = new char[filesize];

		file.seekg(0, std::ios::beg);

		file.read(source, filesize);

		file.close();

	}
	else {

		std::cout << "Error reading file. ";
		system("pause");
		return 0;
	}

	delete filename;

	std::ofstream ofs;
	ofs.open("bytecode.bin", std::ofstream::out | std::ofstream::binary | std::ofstream::trunc);

	Tokenizer tokenizer(source, filesize, " \n\r", 3);

	while (tokenizer.next()) {

		if (tokenizer.compareToken("end"))    { 
			long buf = (long)Opcode::end;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("ipop"))   { 
			long buf = (long)Opcode::ipop;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("ipush"))  { 
			long buf = (long)Opcode::ipush;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("iconst")) {
			long buf = (long)Opcode::iconst;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("ival"))   { 
			long buf = (long)Opcode::ival;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("imov"))   { 
			long buf = (long)Opcode::imov;
			ofs.write((char*)&buf, sizeof(long));
		}

		else if (tokenizer.compareToken("iadd")) {
			long buf = (long)Opcode::iadd;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("isub")) { 
			long buf = (long)Opcode::isub;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("imul")) { 
			long buf = (long)Opcode::imul;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("idiv")) { 
			long buf = (long)Opcode::idiv;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("imod")) { 
			long buf = (long)Opcode::imod;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("iinc")) { 
			long buf = (long)Opcode::iinc;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("idec")) { 
			long buf = (long)Opcode::idec;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("iand")) { 
			long buf = (long)Opcode::iand;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("ior"))  { 
			long buf = (long)Opcode::ior;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("inot")) { 
			long buf = (long)Opcode::inot;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("ineg")) { 
			long buf = (long)Opcode::ineg;
			ofs.write((char*)&buf, sizeof(long));
		}

		else if (tokenizer.compareToken("iles"))  { 
			long buf = (long)Opcode::iles;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("igre"))  { 
			long buf = (long)Opcode::igre;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("iequ"))  { 
			long buf = (long)Opcode::iequ;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("ieles")) { 
			long buf = (long)Opcode::ieles;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("iegre")) { 
			long buf = (long)Opcode::iegre;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("ineq"))  {
			long buf = (long)Opcode::ineq;
			ofs.write((char*)&buf, sizeof(long));
		}

		else if (tokenizer.compareToken("jmp")) { 
			long buf = (long)Opcode::jmp;
			ofs.write((char*)&buf, sizeof(long));
		}
		else if (tokenizer.compareToken("con")) {
			long buf = (long)Opcode::con;
			ofs.write((char*)&buf, sizeof(long)); 
		}
		else if (tokenizer.getTokenLength() == 0) 
			continue;
		else { 
			long buf = atoi(tokenizer.tokenToString());
			ofs.write((char*)&buf, sizeof(long));
		}
	}
	
	ofs.close();

	return 0;
}
