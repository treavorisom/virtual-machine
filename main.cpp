//#include <fstream>
//#include <iostream>
//#include <chrono>
//
//typedef int opcodeType;
//
////int loadFile(const char* fileName, unsigned char* &data, unsigned int &size) {
////	std::ifstream file(fileName, std::ios::in | std::ios::binary | std::ios::ate);
////
////	if (file.is_open()) {
////
////		size = file.tellg();
////		data = new unsigned char[size];
////
////		file.seekg(0, std::ios::beg);
////
////		file.read((char*)data, size);
////
////		file.close();
////	}
////	else
////		return false;
////
////	return true;
////}
//
//
//
////   0  opcode
////  [0] mem at opcode
////  {0} 
////  #0  number
//enum Opcode {
//	END = 0,
//
//	MOV, // [0] = [1]
//	SET, // [0] =  1
//	CLR, // [0] = (0)
//	//REF, // [0] = [1]
//	LOD, //  [0] = M[1]
//	SAV, // M[0] =  [1]
//
//	ADD, // [0] = [1] + [2]
//	SUB, // [0] = [1] - [2]
//	MUL, // [0] = [1] * [2]
//	DIV, // [0] = [1] / [2]
//	MOD, // [0] = [1] % [2]
//
//	INC, // [0]++
//	DEC, // [0]--
//	NOT, // ![0]
//	NEG, // -[0]
//
//	CGR, // [0] = [1] >  [2]
//	CLS, // [0] = [1] <  [2]
//	CGE, // [0] = [1] >= [2]
//	CLE, // [0] = [1] <= [2]
//	CEQ, // [0] = [1] == [2]
//	CNQ, // [0] = [1] != [2]
//
//	JMP, // counter = [0]
//	JRL, // counter += [0]
//	CON, // if ([0]) counter = [1]
//
//	CAL, // call function
//
//
//};
//
//class Stack {
//	unsigned int* bytecode;
//	unsigned int* top;
//
//public:
//	Stack(unsigned int* bytecode) { this->bytecode = bytecode; top = bytecode; }
//
//	void jmp(unsigned int position) { top = (bytecode + position); }
//	void jrl(unsigned int position) { top += position; }
//
//	unsigned int* get() { return top; }
//	unsigned int  pop() { return *top++; }
//	unsigned int  pop(unsigned int num) { top += num; return *(top); }
//	unsigned int  peak() { return *top; }
//	unsigned int  peak(unsigned int pos) { return top[pos]; }
//};
//
//class Memory {
//	unsigned int* memory;
//	unsigned int size;
//
//public:
//	Memory(unsigned int size) { this->size = size; memory = new unsigned int[size]; }
//	~Memory() { delete[] memory; }
//
//	unsigned int& operator[] (int index) {
//		return memory[index];
//	}
//
//	unsigned int get(unsigned int pos) { return memory[pos]; }
//	void set(unsigned int value, unsigned int pos) { memory[pos] = value; }
//};
//
//
//int call(unsigned char id, Stack* stack, Memory* memory) {
//	switch (id)
//	{
//	case 1: // standard out (int)
//		std::cout << (int)(*memory)[stack->pop()];
//		break;
//	case 2: // standard out (char)
//		std::cout << (char)(*memory)[stack->pop()];
//		break;
//	default:
//		break;
//	}
//
//	return 1;
//}
//
//int directAddressing(unsigned int* bytecode) {
//
//	Stack stack(bytecode);
//	Memory memory(1024);
//
//	auto start = std::chrono::high_resolution_clock::now();
//
//	while (stack.peak()) {
//
//		switch (stack.peak()) {
//
//		case Opcode::MOV:
//			memory[stack.peak(1)] = memory[stack.peak(2)];
//			stack.pop(3);
//			break;
//
//		case Opcode::SET:
//			memory[stack.peak(1)] = stack.peak(2);
//			stack.pop(3);
//			break;
//
//		case Opcode::REF:
//			memory[stack.peak(1)] = memory[memory[stack.peak(2)]];
//			stack.pop(3);
//			break;
//
//
//		case Opcode::ADD:
//			memory[stack.peak(1)] = memory[stack.peak(2)] + memory[stack.peak(3)];
//			stack.pop(4);
//			break;
//
//		case Opcode::SUB:
//			memory[stack.peak(1)] = memory[stack.peak(2)] - memory[stack.peak(3)];
//			stack.pop(4);
//			break;
//
//		case Opcode::MUL:
//			memory[stack.peak(1)] = memory[stack.peak(2)] * memory[stack.peak(3)];
//			stack.pop(4);
//			break;
//
//		case Opcode::DIV:
//			memory[stack.peak(1)] = memory[stack.peak(2)] / memory[stack.peak(3)];
//			stack.pop(4);
//			break;
//
//		case Opcode::MOD:
//			memory[stack.peak(1)] = memory[stack.peak(2)] % memory[stack.peak(3)];
//			stack.pop(4);
//			break;
//
//
//		case Opcode::INC:
//			++memory[stack.peak(1)];
//			stack.pop(2);
//			break;
//
//		case Opcode::DEC:
//			--memory[stack.peak(1)];
//			stack.pop(2);
//			break;
//
//		case Opcode::NOT:
//			memory[stack.peak(1)] = !memory[stack.peak(1)];
//			stack.pop(2);
//			break;
//
//		case Opcode::NEG:
//			//memory[stack.peak(1)] = -memory[stack.peak(1)];
//			stack.pop(2);
//			break;
//
//
//		case Opcode::CGR:
//			memory[stack.peak(1)] = memory[stack.peak(2)] > memory[stack.peak(3)];
//			stack.pop(4);
//			break;
//
//		case Opcode::CLS:
//			memory[stack.peak(1)] = memory[stack.peak(2)] < memory[stack.peak(3)];
//			stack.pop(4);
//			break;
//
//		case Opcode::CGE:
//			memory[stack.peak(1)] = memory[stack.peak(2)] >= memory[stack.peak(3)];
//			stack.pop(4);
//			break;
//
//		case Opcode::CLE:
//			memory[stack.peak(1)] = memory[stack.peak(2)] <= memory[stack.peak(3)];
//			stack.pop(4);
//			break;
//
//		case Opcode::CEQ:
//			memory[stack.peak(1)] = memory[stack.peak(2)] == memory[stack.peak(3)];
//			stack.pop(4);
//			break;
//
//		case Opcode::CNQ:
//			memory[stack.peak(1)] = memory[stack.peak(2)] != memory[stack.peak(3)];
//			stack.pop(4);
//			break;
//
//
//		case Opcode::JMP:
//			stack.jmp(memory[stack.peak(1)]);
//			stack.pop(2);
//			break;
//
//		case Opcode::JRL:
//			stack.jrl(memory[stack.peak(1)]);
//			stack.pop(2);
//			break;
//
//		case Opcode::CON:
//			if (memory[stack.peak(1)])
//				stack.jmp(memory[stack.peak(2)]);
//			else
//				stack.pop(3);
//			break;
//
//
//		case Opcode::CAL:
//			stack.pop();
//			call(stack.pop(), &stack, &memory);
//			break;
//
//		default:
//			stack.pop();
//			break;
//
//		}
//	}
//
//	return 0;
//}
//
//void main() {
//	// Get bytecode
//
//	//unsigned char* bytecode;
//	//unsigned int bytecodeLength;
//	//char filename[256];
//
//	//std::cin >> filename;
//
//	//loadFile(filename, bytecode, bytecodeLength);
//
//
//
//	// Run program
//
//	unsigned int bytecode[] = {
//		Opcode::SET, 0, 0,
//		Opcode::SET, 1, 1000000,
//		Opcode::SET, 2, 18,
//		Opcode::SET, 7, (int)'i',
//		Opcode::SET, 6, 34,
//		Opcode::SET, 3, 1000,
//		Opcode::INC, 0,
//		Opcode::CLS, 4, 0, 1,
//		Opcode::MOD, 5, 0, 3,
//		Opcode::CON, 5, 6,
//		Opcode::CAL, 2, 7, 
//		Opcode::CON, 4, 2,
//		Opcode::END,
//	};
//
//	Stack stack(bytecode);
//	Memory memory(1024);
//	
//	auto start = std::chrono::high_resolution_clock::now();
//
//	while (stack.peak()) {
//
//		switch (stack.peak()) {
//
//		case Opcode::MOV:
//			memory[memory[stack.peak(1)]] = memory[memory[stack.peak(2)]];
//			stack.pop(3);
//			break;
//
//		case Opcode::LOD:
//			memory[stack.peak(1)] = stack.peak(2);
//			stack.pop(3);
//			break;
//
//		case Opcode::SET:
//			memory[memory[stack.peak(1)]] = stack.peak(2);
//			stack.pop(3);
//			break; 
//
//
//		case Opcode::ADD:
//			memory[memory[stack.peak(1)]] = memory[memory[stack.peak(2)]] + memory[memory[stack.peak(3)]];
//			stack.pop(4);
//			break; 
//
//		case Opcode::SUB:
//			memory[memory[stack.peak(1)]] = memory[memory[stack.peak(2)]] - memory[memory[stack.peak(3)]];
//			stack.pop(4);
//			break;
//
//		case Opcode::MUL:
//			memory[memory[stack.peak(1)]] = memory[memory[stack.peak(2)]] * memory[memory[stack.peak(3)]];
//			stack.pop(4);
//			break;
//
//		case Opcode::DIV:
//			memory[memory[stack.peak(1)]] = memory[memory[stack.peak(2)]] / memory[memory[stack.peak(3)]];
//			stack.pop(4);
//			break;
//
//		case Opcode::MOD:
//			memory[memory[stack.peak(1)]] = memory[memory[stack.peak(2)]] % memory[memory[stack.peak(3)]];
//			stack.pop(4);
//			break;
//
//
//		case Opcode::INC:
//			++memory[memory[stack.peak(1)]];
//			stack.pop(2);
//			break;
//
//		case Opcode::DEC:
//			--memory[memory[stack.peak(1)]];
//			stack.pop(2);
//			break;
//
//		case Opcode::NOT:
//			memory[memory[stack.peak(1)]] = !memory[memory[stack.peak(1)]];
//			stack.pop(2);
//			break;
//
//		case Opcode::NEG:
//			//memory[stack.peak(1)] = -memory[stack.peak(1)];
//			stack.pop(2);
//			break;
//
//
//		case Opcode::CGR:
//			memory[memory[stack.peak(1)]] = memory[memory[stack.peak(2)]] > memory[memory[stack.peak(3)]];
//			stack.pop(4);
//			break;
//
//		case Opcode::CLS:
//			memory[memory[stack.peak(1)]] = memory[memory[stack.peak(2)]] < memory[memory[stack.peak(3)]];
//			stack.pop(4);
//			break;
//
//		case Opcode::CGE:
//			memory[memory[stack.peak(1)]] = memory[memory[stack.peak(2)]] >= memory[memory[stack.peak(3)]];
//			stack.pop(4);
//			break;
//
//		case Opcode::CLE:
//			memory[memory[stack.peak(1)]] = memory[memory[stack.peak(2)]] <= memory[memory[stack.peak(3)]];
//			stack.pop(4);
//			break;
//
//		case Opcode::CEQ:
//			memory[memory[stack.peak(1)]] = memory[memory[stack.peak(2)]] == memory[memory[stack.peak(3)]];
//			stack.pop(4);
//			break;
//
//		case Opcode::CNQ:
//			memory[memory[stack.peak(1)]] = memory[memory[stack.peak(2)]] != memory[memory[stack.peak(3)]];
//			stack.pop(4);
//			break;
//
//
//		case Opcode::JMP:
//			stack.jmp(memory[memory[stack.peak(1)]]);
//			stack.pop(2);
//			break;
//
//		case Opcode::JRL:
//			stack.jrl(memory[memory[stack.peak(1)]]);
//			stack.pop(2);
//			break;
//
//		case Opcode::CON:
//			if (memory[memory[stack.peak(1)]])
//				stack.jmp(memory[memory[stack.peak(2)]]);
//			else
//				stack.pop(3);
//			break;
//
//
//		case Opcode::CAL:
//			stack.pop();
//			call(stack.pop(), &stack, &memory);
//			break; 
//
//		default:
//			stack.pop();
//			break;
//			
//		}
//	}
//
//	auto elapsed = std::chrono::high_resolution_clock::now() - start;
//	long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
//
//
//	// Memory dump
//
//	std::cout << "\nProgram Terminated\n";
//	std::cout << "Execution Time: " << microseconds << '\n';
//	std::cout << "Register Values: ";
//
//	for (int i = 0; i < 10; i++)
//		std::cout << (int)memory[i] << ' ';
//
//
//
//	start = std::chrono::high_resolution_clock::now();
//
//	int i = 0;
//	while (i < 1000000) {
//		if (!(i%1000))
//			std::cout << 'j';
//		i++;
//	}
//		
//
//	elapsed = std::chrono::high_resolution_clock::now() - start;
//	microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
//	std::cout << "\nExecution Time: " << microseconds << ' ' << i << '\n';
//
//	// End
//	std::cout << '\n'; system("pause");
//}